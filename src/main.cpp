#include "crypto.hpp"
#include "array.hpp"
#include "network.hpp"

#include <iostream>
#include <iomanip>
#include <ostream>
#include <istream>
#include <unistd.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <fstream>

using namespace std;

//Used to encrypt the package data.
array::array *encryptRSA(array::array *data) {

	RSA *server_pk;
	array::array *data_enc;

	server_pk = crypto::rsa_read_public_key_from_PEM("/home/victorsfleite/UnB/OO/EP1/keys/server_pk.pem");

	data_enc = crypto::rsa_encrypt(data, server_pk);

	return data_enc;
}

//Used to decrypt the package data.
array::array *decryptRSA(array::array *data) {

	RSA *client_sk;
	array::array *data_dec;

	client_sk = crypto::rsa_read_private_key_from_PEM("/home/victorsfleite/UnB/OO/EP1/keys/client_sk.pem");

	data_dec = crypto::rsa_decrypt(data, client_sk);

	return data_dec;
}

//Used to establish the connection between the client and the server.
int connect() {

	int fd;

	cout << "\t>> Inicializing connection to the server..." << endl;

	if((fd = network::connect("45.55.185.4", 3000))<0) {
		cout << "\t>> Connection Failed!" << endl;
	} else{
		cout << "\t>> Connection to the Server " << fd << " successful!" << endl;
	}

	return fd;
}

//Used to receive packages from the server.
array::array* receive_package(int fd, array::array *package_sent) {

	array::array *package_received;

	network::write(fd, package_sent);

	if((package_received = network::read(fd)) == nullptr) {
		cout << "\t>> Reading package failed!" << endl;
	}

	array::destroy(package_sent);

	return package_received;
}

//Used to create an empty package.
array::array* create_empty_package(byte tag) {
	
	array::array *empty_package; 

	empty_package = array::create(7);
				
		empty_package->data[0] = 0x03;
		empty_package->data[1] = 0;
		empty_package->data[2] = 0;
		empty_package->data[3] = 0;
		empty_package->data[4] = tag;
		empty_package->data[5] = 0;
		empty_package->data[6] = 0;

		return empty_package;
}

//Used to create the entire package (package length, tag, value length, value and hash);
array::array *create_entire_package(byte PL1, byte PL2, byte PL3, byte PL4, byte tag, byte CL1, byte CL2, array::array* content) {

	array::array *entire_package;
	array::array *hash;

	entire_package = array::create(27 + content->length);

	entire_package->data[0] = PL1;
	entire_package->data[1] = PL2;
	entire_package->data[2] = PL3;
	entire_package->data[3] = PL4;
	entire_package->data[4] = tag;
	entire_package->data[5] = CL1;
	entire_package->data[6] = CL2;

	hash = crypto::sha1(content);
	memcpy(entire_package->data+7, content->data, content->length);
	memcpy(entire_package->data + 7 + content->length, hash->data, 20);

	array::destroy(hash);

	return entire_package;
}

//Function to execute the registration protocol.
array::array *registration(array::array *content, int fd) {

	array::array *REQUEST_REGISTRATION;
	array::array *REGISTRATION_START;
	array::array *REGISTER_ID;
	array::array *REGISTERED;
	array::array *s_enc;
	array::array *s_dec;

	REQUEST_REGISTRATION = create_empty_package(0xC0);

	REGISTRATION_START = receive_package(fd, REQUEST_REGISTRATION);
	if(REGISTRATION_START->data[4] == 0xC1){
		cout << "\t>> REGISTRATION_START package received!" << endl;
	}

	REGISTER_ID = create_entire_package(0x17, 0x02, 0, 0, 0xC2, 0x00, 0x02, content);

	REGISTERED = receive_package(fd, REGISTER_ID);
	if(REGISTERED->data[4] == 0xC3){
		cout << "\t>> REGISTERED package received!" << endl;
	}

	s_enc = array::create(512);

	memcpy(s_enc->data, REGISTERED->data + 7, 512);

	s_dec = decryptRSA(s_enc);

	return s_dec;
}

//Executes the authentication protocol.
array::array *authentication(array::array *content, array::array *s_dec, int fd) {

	array::array *REQUEST_AUTH;
	array::array *AUTH_START;
	array::array *REQUEST_CHALLENGE;
	array::array *CHALLENGE;
	array::array *AUTHENTICATE;
	array::array *AUTHENTICATED;

	array::array *A_enc;
	array::array *A_dec;
	array::array *M_enc;
	array::array *M_dec;
	array::array *T_enc;
	array::array *T_dec;

	REQUEST_AUTH = create_entire_package(0x17, 0x02, 0, 0, 0xA0, 0x00, 0x02, content);

	AUTH_START = receive_package(fd, REQUEST_AUTH);
	if(AUTH_START->data[4] == 0xA1){
		cout << "\t>> AUTH_START package received!" << endl;
	}

	A_enc = array::create(AUTH_START->length-27);
	memcpy(A_enc->data, AUTH_START->data + 7, AUTH_START->length-27);
	A_dec = decryptRSA(A_enc);

	REQUEST_CHALLENGE = create_empty_package(0xA2);

	CHALLENGE = receive_package(fd, REQUEST_CHALLENGE);
	if(CHALLENGE->data[4] == 0xA4){
		cout << "\t>> CHALLENGE package received!" << endl;
	}

	M_enc = array::create(CHALLENGE->length-27);
	memcpy(M_enc->data, CHALLENGE->data + 7, CHALLENGE->length-27);
	M_dec = crypto::aes_decrypt(M_enc, A_dec, s_dec);

	AUTHENTICATE = create_entire_package(0x27, 0, 0, 0, 0xA5, 0x10, 0, M_dec);

	AUTHENTICATED = receive_package(fd, AUTHENTICATE);
	if(AUTHENTICATED->data[4] == 0xA6){
		cout << "\t>> AUTHENTICATED package received!" << endl;
	}

	T_enc = array::create(AUTHENTICATED->length-27);
	memcpy(T_enc->data, AUTHENTICATED->data + 7, AUTHENTICATED->length-27);
	T_dec = crypto::aes_decrypt(T_enc, A_dec, s_dec);

	array::destroy(A_dec);
	array::destroy(A_enc);

	return T_dec;
}

//Executes the request protocol.
array::array *request(array::array *content, array::array *T_enc, array::array *s_dec, int fd) {

	int object_length;

	array::array *REQUEST_OBJECT;
	array::array *OBJECT;
	array::array *objectID_enc;
	array::array *obj_enc;
	array::array *obj_dec;

	objectID_enc = crypto::aes_encrypt(content, T_enc, s_dec);

	REQUEST_OBJECT = create_entire_package(0x27, 0, 0, 0, 0xB0, 0x10, 0, objectID_enc);

	network::write(fd, REQUEST_OBJECT);
	
	if((OBJECT = network::read(fd, 16167)) == nullptr){
		cout << "\t>> Reading null!" << endl;
	} else{
		cout << "\t>> OBJECT with "<< OBJECT->length << " bytes received!" << endl;
	}

	object_length = 16167-23;

	obj_enc = array::create(object_length);
	memcpy(obj_enc->data, OBJECT->data + 7, object_length);
	obj_dec = crypto::aes_decrypt(obj_enc, T_enc, s_dec);

	return obj_dec;
}

//Creates the file on local storage.
void create_object(array::array *obj_dec) {

	ofstream image;
		image.open ("/home/victorsfleite/UnB/OO/EP1/cliente_seguro/doc/image.jpg", ios::binary); // trocar o local da  image
	if( !image ){
		cout << "\t>> Creating file error!";
	} else{
		cout << "\t>> Creating the file...\n" << endl;
		sleep(2);
		for (int i = 0; i < (int)obj_dec->length; i++){
			image << obj_dec->data[i];
		}

	}
	image.close();

}

int main(int argc, char const *argv[]){

	int fd;
	
	fd = connect();

	byte id[] = {0x42, 0x6a, 0xb1, 0xb4, 0xb2, 0x81, 0x99, 0xef};
	byte *iD = id;
	array::array *ID = array::create(sizeof(id), iD);
	
	byte obj[] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
	byte *Obj = obj;
	array::array *OBJ = array::create(sizeof(obj), Obj);

	array::array *ID_enc = encryptRSA(ID);
		
	array::array *s_dec = registration(ID_enc, fd);

	array::array *T_enc = authentication(ID_enc, s_dec, fd);

	array::array *objeto =  request(OBJ, T_enc, s_dec, fd);

	create_object(objeto);

 	return 0;
}
